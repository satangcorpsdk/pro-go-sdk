module pro-go-sdk

go 1.13

require (
	github.com/sirupsen/logrus v1.4.2
	gopkg.in/resty.v1 v1.12.0
)
